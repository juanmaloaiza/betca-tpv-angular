export interface CashierStatus {
  salesTotal: number;
  totalCard: number;
  totalCash: number;
  totalVoucher: number;
}
